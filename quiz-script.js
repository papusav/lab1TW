var currentQuestion = 0;
var score = 0;
var totQuestions = questions.length;

var container = document.getElementById('quizContainer');
var questionEl = document.getElementById('question');
var opt1 = document.getElementById('opt1');
var opt2 = document.getElementById('opt2');


var newxtButton = document.getElementById('nextButton');
var resultCont = document.getElementById('result');
var message = document.getElementById('message');

function loadQuestion(questionIndex) {
    var q = questions[questionIndex];
    questionEl.textContent = (questionIndex + 1) + '. ' + q.question;
    opt1.textContent = q.option1;
    opt2.textContent = q.option2;
};

function loadNextQuestion() {
    var selectedOption = document.querySelector('input[type=radio]:checked');
    if (!selectedOption) {
        alert('Please select your answer!');
        return;
    }
    var answer = selectedOption.value;
    if (questions[currentQuestion].answer == answer) {
        score += 16.66;
    }
    selectedOption.checked = false;
    currentQuestion++;

    if (currentQuestion == totQuestions - 1) {
        nextButton.textContent = 'Finish';
    }

    if (currentQuestion == totQuestions) {
        container.style.display = 'none';

        var messages = ["Great job!", "That's just okay", "You really need to do better"];

        if (score < 10) {
            resultMessage = messages[2];
        } else {
            if (score > 9 && score < 90) {
                resultMessage = messages[1];
            } else {
                resultMessage = messages[0];
                score = 100;
            }
        }

        resultCont.style.display = '';
        resultCont.textContent = 'Your Score ' + score;
        message.textContent = resultMessage;

        return;
    }

    loadQuestion(currentQuestion);
}

loadQuestion(currentQuestion);